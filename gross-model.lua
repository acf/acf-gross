local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")

-- Set variables
local configfile = "/etc/grossd.conf"
local processname = "grossd"
local packagename = "gross"

local path = "PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin "

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Gross Status")
end

function mymodule.getstatusdetails()
	return cfe({ type="longtext", value="", label="Gross Status Details" })
end

function mymodule.get_filedetails()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filedetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_logfile(self, clientdata)
	local retval = cfe({ type="group", value={}, label="Log File Configuration" })
	retval.value.facility = cfe({value=format.parse_ini_file(fs.read_file(configfile), "", "syslog_facility") or "mail", label="Syslog Facility"})
	retval.value.grep = cfe({ value="gross", label="Grep" })
	return retval
end

return mymodule
